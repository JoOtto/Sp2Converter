﻿using System;
using System.Collections.Generic;
using System.Drawing.Text;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Sp2Converter
{
    
    public class Sp2File
    {
        List<string> Header = new List<string>();
        List<string> SecondHeader = new List<string>();
        private int RawXsize;
        private int RawYsize;
        double[] RawData;
        double[] CalcData;

        public Sp2File(string PathToVersion102)
        {
            //Read File
            //string[] content = File.ReadAllLines(PathToVersion102);

            File.ReadAllText(PathToVersion102);
            string[] content = File.ReadAllText(PathToVersion102).Split('\n');

            Header.Clear();
            Header.Add(content[0]);
            //Change Format in New File
            if (content[1] == "# [SPECS P2 Imagefile Version 101]")
            {
                throw new System.ArgumentException("File has already version 101");
            }
            content[1] = "# [SPECS P2 Imagefile Version 101]";

            //Read Header
            int startindex = 1;
            for (int i = 1; i < content.Length; i++)
            {
                Header.Add(content[i]);
                if (content[i][0] != '#')
                {
                    startindex = i;
                    break;
                }
            }
            



            //Read Data
            RawXsize = Int32.Parse(content[startindex].Split(' ')[0]);
            RawYsize = Int32.Parse(content[startindex].Split(' ')[1]);
            int IndexOfFirstDataLine = startindex + 1;
            //Read SecondHeaderGap
            SecondHeader.Clear();
            SecondHeader = content.ToList().GetRange(IndexOfFirstDataLine + RawXsize*RawYsize, 2);
            //Allocate Arrays with fixed length
            RawData = new double[RawXsize * RawYsize];
            CalcData = new double[RawXsize * RawYsize];

            IFormatProvider formatProvider = CultureInfo.CreateSpecificCulture("en-US");
            //Optimzed parallel processing of string to double
            int degreeOfParallelism = Environment.ProcessorCount;

            Parallel.For(0, degreeOfParallelism, workerId =>
            {
                var max = RawData.Length * (workerId + 1) / degreeOfParallelism;
                for (int i = RawData.Length * workerId / degreeOfParallelism; i < max; i++)
                    RawData[i] = Convert.ToDouble(content[i + IndexOfFirstDataLine], formatProvider);
            });
            Parallel.For(0, degreeOfParallelism, workerId =>
            {
                var max = CalcData.Length * (workerId + 1) / degreeOfParallelism;
                for (int i = CalcData.Length * workerId / degreeOfParallelism; i < max; i++)
                    CalcData[i] = Convert.ToDouble(content[i + IndexOfFirstDataLine + CalcData.Length + 2], formatProvider);
            });

            
        }



        public void ConvertTo101(string Path)
        {
            int MaxSignificantDigitsRawData = 0;
            for (int i = 0; i < RawData.Length; i++)
            {
                if (RawData[i] != 0)
                {
                    int SignificantDigits = GetSignificantNumberOfDecimalPlaces(RawData[i]);
                    if (SignificantDigits > MaxSignificantDigitsRawData)
                    {
                        MaxSignificantDigitsRawData = SignificantDigits;
                    }
                }
            }
            int MaxSignificantDigitsCalcData= 0;
            for (int i = 0; i < CalcData.Length; i++)
            {
                if (CalcData[i] != 0 )
                {
                    int SignificantDigits = GetSignificantNumberOfDecimalPlaces(CalcData[i]);
                    if (SignificantDigits > MaxSignificantDigitsCalcData)
                    {
                        MaxSignificantDigitsCalcData = SignificantDigits;
                    }
                }
            }
            List<string> NewLines = new List<string>();
            
            Header.RemoveRange(44, Header.Count - 44 -1 );
            NewLines = Header;
            NewLines.AddRange(RawData.Select(x =>((Int64)(x*Math.Pow(10,MaxSignificantDigitsRawData))).ToString()));
            NewLines.AddRange(SecondHeader);
            NewLines.AddRange(CalcData.Select(x => ((Int64)(x * Math.Pow(10, MaxSignificantDigitsCalcData))).ToString()));

            //Write All Lines Back
            File.WriteAllLines(Path, NewLines);

        }
        static int GetSignificantNumberOfDecimalPlaces(double d)
        {
            string inputStr = d.ToString(CultureInfo.InvariantCulture);
            int decimalIndex = inputStr.IndexOf(".") + 1;
            if (decimalIndex == 0)
            {
                return 0;
            }
            return inputStr.Substring(decimalIndex).TrimEnd(new[] { '0' }).Length;

        }

    }
}