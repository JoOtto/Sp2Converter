﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sp2Converter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }



        private void button1_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] filePaths = Directory.GetFiles(fbd.SelectedPath, "*.sp2", SearchOption.TopDirectoryOnly);
                    foreach (string path in filePaths)
                    {
                        try
                        {
                            textBox1.Text += "++ Try convert " + path + " ...\r\n";
                            textBox1.Refresh();
                            Sp2File sp2File = new Sp2File(path);

                            sp2File.ConvertTo101(path.Split('.')[0] + "_conv.sp2");
                            textBox1.Text += "++ File finished\r\n";
                        }
                        catch (Exception ex)
                        {
                            if(ex.Message == "File has already version 101") textBox1.Text += "++ File has already version 101\r\n";
                            else
                            textBox1.Text += ex.ToString();
                        }
                       
                        
                    }
                    textBox1.Text += "-----\r\n Convert finished!\r\n ---\r\n";
                    textBox1.Refresh();

                }
            }


        }
    }
}
